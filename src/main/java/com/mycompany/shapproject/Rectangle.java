/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapproject;

/**
 *
 * @author sairu
 */
public class Rectangle extends Shape {

    private double wide;
    private double high;

    public Rectangle(double w, double h) {
        super("Rectangle");
        this.wide = w;
        this.high = h;
    }

    public double getW() {
        return wide;
    }

    public double getH() {
        return high;
    }

    public void setW(double w) {
        this.wide = w;
    }

    public void setH(double h) {
        this.high = h;
    }

    @Override
    public double calArea() {
        return wide * high;
    }

    @Override
    public double calPerimeter() {
        return (wide * 2) + (high * 2);
    }

}
