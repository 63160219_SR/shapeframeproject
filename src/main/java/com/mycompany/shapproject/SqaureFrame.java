/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author sairu
 */
public class SqaureFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Sqaure");
        frame.setSize(350, 350);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblSide = new JLabel("side", JLabel.TRAILING);
        lblSide.setSize(50, 20);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        lblSide.setLocation(5, 5);
        frame.add(lblSide);

        final JTextField txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(60, 5);
        frame.add(txtSide);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Sqaure side = ??? area = ??? permeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSide = txtSide.getText();
                    double side = Double.parseDouble(strSide);
                    Sqaure sqaure = new Sqaure(side);
                    lblResult.setText("Sqaure side = " + String.format("%.2f", sqaure.getSide()) + " area = "
                            + String.format("%.2f", sqaure.calArea()) + " perimater = " + String.format("%.2f", sqaure.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });

        frame.setVisible(true);
    }
}
