/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author sairu
 */
public class TriangleFrame {
     public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblBase = new JLabel("base", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        lblBase.setLocation(5, 5);
        frame.add(lblBase);

        final JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        frame.add(txtBase);

        JLabel lblHigh = new JLabel("high", JLabel.TRAILING);
        lblHigh.setSize(50, 20);
        lblHigh.setBackground(Color.WHITE);
        lblHigh.setOpaque(true);
        lblHigh.setLocation(5, 30);
        frame.add(lblHigh);
        
        final JTextField txtHigh = new JTextField();
        txtHigh.setSize(50, 20);
        txtHigh.setLocation(60, 30);
        frame.add(txtHigh);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 15);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Base = ??? High = ??? area = ??? permeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(450, 60);
        lblResult.setLocation(0, 55);
        lblResult.setBackground(Color.cyan);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strBase = txtBase.getText();
                double Base = Double.parseDouble(strBase);
                String strHigh = txtHigh.getText();
                double High = Double.parseDouble(strHigh);
                Triangle triangle = new Triangle(Base,High);
                lblResult.setText("Triangle base = "+String.format("%.2f", triangle.getBase())+" Triangle high = "
                        +String.format("%.2f", triangle.getHigh())+" area = "
                        +String.format("%.2f", triangle.calArea())+" perimater = "+String.format("%.2f", triangle.calPerimeter()));
            }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
            txtBase.setText("");
            txtBase.requestFocus();
            txtHigh.setText("");
            txtHigh.requestFocus();
            }
            }
        });
        
        frame.setVisible(true);
    }
}
