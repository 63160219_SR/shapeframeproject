/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author sairu
 */
public class RectangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Rectangle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblWide = new JLabel("wide", JLabel.TRAILING);
        lblWide.setSize(50, 20);
        lblWide.setBackground(Color.WHITE);
        lblWide.setOpaque(true);
        lblWide.setLocation(5, 5);
        frame.add(lblWide);

        final JTextField txtWide = new JTextField();
        txtWide.setSize(50, 20);
        txtWide.setLocation(60, 5);
        frame.add(txtWide);

        JLabel lblHigh = new JLabel("high", JLabel.TRAILING);
        lblHigh.setSize(50, 20);
        lblHigh.setBackground(Color.WHITE);
        lblHigh.setOpaque(true);
        lblHigh.setLocation(5, 30);
        frame.add(lblHigh);
        
        final JTextField txtHigh = new JTextField();
        txtHigh.setSize(50, 20);
        txtHigh.setLocation(60, 30);
        frame.add(txtHigh);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 15);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Wide = ??? High = ??? area = ??? permeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(450, 60);
        lblResult.setLocation(0, 55);
        lblResult.setBackground(Color.cyan);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strWide = txtWide.getText();
                double Wide = Double.parseDouble(strWide);
                String strHigh = txtHigh.getText();
                double High = Double.parseDouble(strHigh);
                Rectangle rectangle = new Rectangle(Wide,High);
                lblResult.setText("Rectangle wide = "+String.format("%.2f", rectangle.getW())+" Rectangle high = "
                        +String.format("%.2f", rectangle.getH())+" area = "
                        +String.format("%.2f", rectangle.calArea())+" perimater = "+String.format("%.2f", rectangle.calPerimeter()));
            }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame, "Error: Please input number", "Error", JOptionPane.ERROR_MESSAGE);
            txtWide.setText("");
            txtWide.requestFocus();
            txtHigh.setText("");
            txtHigh.requestFocus();
            }
            }
        });
        
        frame.setVisible(true);
    }
}
